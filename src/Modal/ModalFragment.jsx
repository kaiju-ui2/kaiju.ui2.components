import React from "react";
import Modal from "./Modal";
import "./Modal.scss"
import {getTranslation} from "utils";

export default class ModalFragment extends React.Component {
    constructor(props) {
        super(props);
        this.confirmCallback = props.confirmCallback;
        this.cancelCallback = props.cancelCallback;
        this.state = {show: this.props.show || false};
    }

    onCancel() {
        this.cancelCallback ? this.cancelCallback() : undefined;
        this.setState({show: false})
    }

    onConfirm() {
        try {
            this.confirmCallback ? this.confirmCallback() : undefined;
            this.setState({show: false})
        } catch (e) {
            console.log("error - modalFragment.onConfirm: ", e)
        }
    }

    renderHeader() {
        return (
            <React.Fragment>
                <div className="modal-header">
                    <div className={`modal-cancel ${this.props.closeClassName || ''}`} onClick={() => this.onCancel()}>
                        <i className="icon-cross pointer m--icon-font-size-lg4"/>
                    </div>
                    <div className="modal-ok">
                        {
                            this.confirmCallback && <button
                                type="button"
                                onClick={() => this.onConfirm()}
                                className="btn m-btn--pill btn-success">
                                {getTranslation("Button.confirm")}
                            </button>
                        }
                        {this.props.warningLabel && (
                            <div style={{
                                position: "absolute",
                                right: "2.3rem",
                                width: "400px",
                                textAlign: "right",
                            }}>
                                <i className="icon-warn pr-3" style={{color: "orange", position: "relative", top: "2px"}}/>
                                {this.props.warningLabel}
                            </div>
                        )}
                    </div>
                    <div className="text-center mr-auto ml-auto p-3">
                        <h1 className="">{this.props.label}</h1>
                        <h5 className="">{this.props.labelInfo}</h5>
                    </div>
                </div>
            </React.Fragment>
        )
    }

    render() {
        return (
            <React.Fragment>
                {
                    this.state.show && <Modal>
                        <div className={`m-portlet window ${this.props.className || ''}`}>
                            {this.renderHeader()}
                            {this.props.children}
                        </div>
                    </Modal>
                }
            </React.Fragment>
        )
    }

}


class TransparentModalFragment extends ModalFragment {

    render() {
        return (
            <React.Fragment>
                {
                    this.props.show && <Modal>
                        <div
                            className={`window transparent-window ${this.props.className || ''}`}>
                            <div className={`transparent-window-content ${this.props.contentClassName || ''}`}>
                                {this.props.children}
                            </div>
                        </div>
                    </Modal>
                }
            </React.Fragment>
        )
    }
}


class TransparentModal extends React.Component {

    componentDidMount() {
        document.addEventListener('mouseup', (e) => this.handleClickOutside(e));
    }

    handleClickOutside(event) {
        if (!this.props.show) {
            return
        }

        if (this.ModalRef && !this.ModalRef.contains(event.target)) {
            this.props.cancelCallback()
        }
    }

    render() {
        const classNames = `m-portlet__body ml-auto mr-auto action-modal mw-500 ${this.props.className || ""}`;

        return (
            <React.Fragment>
                {
                    this.props.show &&
                    <TransparentModalFragment show={true}
                                              contentClassName="window-from-content disable-scroll">
                        <div className={classNames}>
                            <div className="row action-modal-content p-5" ref={ref => this.ModalRef = ref}>
                                <div className="action-modal-cross"
                                     onClick={() => this.props.cancelCallback()}>
                                    <i className="icon-cross pointer m--icon-font-size-lg4"/>
                                </div>
                                {this.props.children}
                            </div>
                        </div>
                    </TransparentModalFragment>
                }
            </React.Fragment>
        )
    }
}

export {TransparentModalFragment, TransparentModal}