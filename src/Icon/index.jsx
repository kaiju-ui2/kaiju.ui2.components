import React, { Component } from "react";
import './Icon.scss'

class Icon extends Component {
    render() {
        const { id, fill, className } = this.props;
        const src = `/node_modules/@kaiju.ui2/components/src/Icon/basic.svg#${id}`

        return (
            <div className={`icon-item ${className}`}>
                <div className="icon-item__graphics">
                    <svg className="icon" fill={fill}>
                        <use
                            xmlnsXlink="http://www.w3.org/1999/xlink"
                            xlinkHref={src}
                        />
                    </svg>
                </div>
            </div>
        );
    }
}

export default Icon;
