import React from 'react'
import Header from "./base";
import DynamicHeader from "./DynamicHeader";
import "./style.scss"

/* Хедер, который находится под основным хедером
 *
 */
class SemiHeader extends React.Component {

    render() {

        return (
            <div className={`semi-header ${this.props.className || ""}`}
                 style={(this.props.style || {})}>
                {this.props.children}
            </div>
        )
    }
}

export {Header, DynamicHeader, SemiHeader}