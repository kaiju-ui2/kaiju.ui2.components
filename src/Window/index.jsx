import WindowActionComponent from "./ActionComponent";


class WindowDeleteComponent extends WindowActionComponent {
    imageHref="/static/img/delete.png";
    confirmButtonClass = "btn-danger"
}

class WindowCreateComponent extends WindowActionComponent {
    iconClass = "icon-create";
}

class WindowAlertComponent extends WindowActionComponent {
    iconClass = "icon-pencil";
}

export {WindowDeleteComponent, WindowCreateComponent, WindowAlertComponent}