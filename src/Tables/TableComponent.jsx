import {inject, observer} from "mobx-react";
import React from "react";
import {uuid4} from "utils";
import {toJS} from "mobx";
import PaginationComponent from "./Pagination";
import CellComponent from "./CellComponent";
import CheckboxComponent from "Checkbox";
import "./TableComponent.scss"

@inject("tableStore")
@observer
class HeaderComponent extends React.Component {

    render() {
        return (
            <thead className="thead-inverse">
            <tr>
                {
                    this.props.tableStore.checkable &&
                    <td className="product-table-checkbox" key={uuid4()} onClick={(e) => (e.stopPropagation())}>
                        {this.props.tableStore.checkMulti &&
                        <
                            CheckboxComponent
                            onChange={value => {
                                this.props.tableStore.actions.toggleCheckedAllPage(value);
                            }}

                            defaultChecked={this.props.tableStore.actions.allPageChecked || false}
                        />
                        }
                    </td>
                }
                {
                    this.props.tableStore.header.map((el) => {
                        return (<th key={uuid4()}>{el.label}</th>)
                    })
                }

                {
                    this.props.tableStore.ActionsComponent &&
                    this.props.tableStore.header.length > 0 &&
                    <th key={uuid4()}/>
                }
            </tr>
            </thead>
        )
    }
}

@inject("tableStore")
@observer
class GroupHeaderComponent extends React.Component {
    render() {
        let {groupStore, tableStore} = this.props;
        let colSpan = tableStore.header.length + (this.props.tableStore.ActionsComponent ? 1 : 0);

        return (
            <tr onClick={() => groupStore.toggleGroup()}>
                <td colSpan={colSpan} className="group-header">
                    <i className={!groupStore.isToggled ? "icon-square_minus" : "icon-square_plus"}
                       style={{fontSize: "12px"}}
                    />
                    <h5 style={{display: "inline-block"}} className="pl-2">
                        {groupStore.label}
                    </h5>
                </td>
            </tr>
        )
    }
}


@observer
class TableBodyComponent extends React.Component {
    render() {
        // Accordion - если accordion=True придет GroupStore
        let {groupStore = {}} = this.props;

        return (
            <tbody className={groupStore.isToggled ? "group-collapsed" : ""}>
            {
                (groupStore.label && groupStore.label.length) &&
                <GroupHeaderComponent key={uuid4()} groupStore={groupStore}/>
            }
            {this.props.children}
            </tbody>

        )
    }
}

@observer
class RowCheckboxComponent extends React.Component {
    render() {
        const {rowStore} = this.props;

        return (
            <td className="product-table-checkbox" key={uuid4()}
                onClick={(e) => {
                    e.stopPropagation();
                    rowStore.toggleChecked()
                }}>
                <CheckboxComponent
                    onChange={() => {
                        rowStore.toggleChecked();
                    }}
                    defaultChecked={rowStore.checked || false}/>
            </td>
        )
    }
}

/*
in the table rpc response:

pagination...
data: ...,
count: ...
footer: {
  cell2: 300,
  cell5: 500
}  - it uses default table cell constructor and header to build footer.

 */
@inject("tableStore")
class TableFooter extends React.Component {

    renderFooterCell(header, i) {
        const field = header["key"];
        let value = this.props.tableStore.footer[field];

        if (i === 0) {
            return <td className="v-align-center" key={uuid4()}>
                {utils.getTranslation("Footer.total")}
            </td>
        }

        let defaultConstructor = this.props.tableStore.cellConstructors["default"];
        value = defaultConstructor.bind(this)({value: value});
        return (
            <td className="v-align-center" key={uuid4()}>
                {value}
            </td>
        )
    }

    render() {
        return (
            <tfoot>
            <tr>
                {this.props.tableStore.checkable && <td/>}
                {this.props.tableStore.header.map((header, i) => this.renderFooterCell(header, i))}
            </tr>
            </tfoot>
        )
    }
}

@inject("tableStore")
class TableBody extends React.Component {


    renderRows(rows) {
        // rows в tableStore-e заворачиваются в observable map
        // в каждом элементе rows находится RowStore
        return (
            <React.Fragment>
                {rows.map((rowStore) => {
                        return (
                            <tr className="attribute-row show-hover"
                                onClick={() => {
                                    this.props.tableStore.rowCallback &&
                                    this.props.tableStore.rowCallback(rowStore)
                                }} key={uuid4()}>
                                {
                                    this.props.tableStore.checkable &&
                                    <RowCheckboxComponent rowStore={rowStore}/>
                                }

                                {this.props.tableStore.header.map((header) => {
                                    return <CellComponent
                                        key={uuid4()}
                                        header={header} row={toJS(rowStore.row)}/>
                                })}

                                {
                                    this.props.tableStore.ActionsComponent &&
                                    <td className="v-align-center"
                                        onClick={(e) => (e.stopPropagation())}>
                                        <this.props.tableStore.ActionsComponent
                                            rowStore={rowStore}
                                            tableStore={this.props.tableStore}/>
                                    </td>
                                }

                            </tr>
                        )
                    }
                )}
            </React.Fragment>
        )
    }

    render() {
        return (
            <React.Fragment>
                {
                    this.props.tableStore.data.map((group) => {
                        // group - groupStore при accordion=True
                        return (
                            <TableBodyComponent groupStore={group} key={uuid4()}>
                                {this.renderRows(group.rows)}
                            </TableBodyComponent>
                        )
                    })
                }
            </React.Fragment>
        )
    }
}


@inject("tableStore")
@observer
export default class MainTableComponent extends React.Component {
    constructor(props) {
        super(props);
        this.className = this.props.className || "";
        this.paginationClassName = this.props.paginationClassName || "";
    }

    componentDidMount() {
        this.props.tableStore.actions.startRefresh()
    }

    componentWillUnmount() {
        this.props.tableStore.actions.stopRefresh()
    }

    render() {
        return (
            <React.Fragment>
                {
                    !this.props.tableStore.disablePagination &&
                    <PaginationComponent className={this.paginationClassName}/>
                }

                <div className={
                    this.props.tableStore.actions.isFetching === true
                    || this.props.tableStore.actions.page === 0 ? `product-table-loader ${this.props.className}` : this.props.className
                }>
                    <table className="table table-hover table-checkable dataTable no-footer" id={uuid4()}>
                        {!this.props.tableStore.conf.disableHeader && <HeaderComponent/>}
                        <TableBody/>
                        {this.props.tableStore.footer && <TableFooter/>}
                    </table>
                </div>
            </React.Fragment>
        )
    }
}


class TableDropdowns extends React.Component {

    render() {
        return (
            <div className={`table-dropdowns ${this.props.className || ""}`}>
                {this.props.children}
            </div>
        )
    }

}

export {
    TableDropdowns
}