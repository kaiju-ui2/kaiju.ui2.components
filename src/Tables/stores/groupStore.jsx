import RowStore from "./rowStore";
import {action, computed} from "mobx";

export default class GroupStore {

    constructor(actionsStore, data) {
        this.actionsStore = actionsStore;
        this.label = data.label;
        this.rows = data.rows.map((row) => new RowStore(row, this.actionsStore.store));
        // its important that group need to have "id"
        // key to be collapsed after reloading
        this.id = data.id
    }

    @computed
    get isToggled() {
        return this.actionsStore.toggledGroups.has(this.id)
    }

    @action
    toggleGroup() {
        if (this.actionsStore.toggledGroups.has(this.id)) {
            this.actionsStore.toggledGroups.delete(this.id)
        } else {
            this.actionsStore.toggledGroups.add(this.id)
        }
    }
}