import BaseStorage from 'stores/storage'
import tableCellConstructors, {tableFieldConstructors} from "Tables/constructors";
import {action, computed, observable, runInAction, toJS} from "mobx";
import axios from 'axios';
import RowStore from "./rowStore";
import GroupStore from "./groupStore";
import {getTranslation, handleNotifyExceptions} from "utils";

class TableActions {
    @observable isFetching;
    @observable page;
    @observable pages;
    @observable toggledGroups = new Set(); // for accordion
    @observable count;

    @observable allCheckedMode = false;

    constructor(store) {
        this.store = store;

        this.page = 0;
        this.pages = 1;
        this.isFetching = false;

        if (this.store.init) {
            this.fetch();
        }
    }

    @action
    fetchByPage(page) {
        if (!page) {
            page = this.page
        }

        let pages = this.pages || 1;

        if ((page <= pages) && !this.isFetching) {
            this._fetchTable(page)
        }
    }

    @computed
    get filters() {
        if (!this.store.conf.request.params) {
            return []
        }

        return this.store.conf.request.params.filters || [];
    }

    @computed
    get query() {
        if (!this.store.conf.request.params) {
            return []
        }

        return this.store.conf.request.params.query || "";
    }


    @action
    fetchByFilters(filters) {
        if (filters.length === 0 && this.store.conf.request.params.filters.length === 0) {
            return
        }
        this.store.conf.request.params.filters = filters;
        this.fetchByPage(1)
    }

    @action
    fetchById(id) {
        this.store.conf.request.params.id = id;
        this.fetchByPage(1)
    }

    @action
    fetchByQuery(value) {
        this.store.conf.request.params.query = value;
        this.fetchByPage(1)
    }

    @action
    _fetchTable(page) {
        this.isFetching = true;
        let params = this.store.conf.request.params;

        axios.post("/public/rpc",
            {
                "method": this.store.conf.request.method,
                "params": {
                    page: page,
                    ...params,
                }
            }
        ).then(response => {
            runInAction(() => {
                if (!response.data.result) {
                    if (this.store.conf.request.errorCallback) {
                        this.store.conf.request.errorCallback(response.data);
                    }
                    return
                }

                let data;
                this.pages = response.data.result.pagination.pages;
                this.page = response.data.result.pagination.page;
                this.count = response.data.result.count;

                let result = response.data.result;

                let header = result["header"];

                if (!this.store.header.length) {
                    if (header) {
                        this.store.header = header
                    } else {
                        this.store.header = result.fields.map(function (key) {
                            return {
                                "key": key,
                                "label": getTranslation(`Field.${key}`)
                            }
                        });
                    }
                }

                let footer = result["footer"]
                if (!this.store.footer && footer) {
                    this.store.footer = footer
                }

                // оборачиваем rows в RowStore, чтобы ловить в дальнейшем действия чекбокса
                if (this.store.accordion) {
                    data = result.data.map((groupRows) => new GroupStore(this, groupRows));
                } else {
                    data = [{"rows": result.data.map((row) => (new RowStore(row, this.store)))}];
                }
                this.store.data = data;

                this.isFetching = false;
            })
        }).catch(response => {
            this.isFetching = false;

            if (this.store.conf.request.errorCallback) {
                this.store.conf.request.errorCallback(response.data)
            } else {
                handleNotifyExceptions(response)
            }

            handleNotifyExceptions(response)
        })
    }

    get checkedIds() {
        return Object.keys(toJS(this.store.checkedRows))
    }

    get checkedData() {
        // note: not working with accordion table
        const checkedIds = this.checkedIds;

        if (!this.store.data[0]) {
            return []
        }

        return this.store.data[0].rows.filter(row => checkedIds.includes(row.id))
    }

    get unCheckedIds() {
        return Object.keys(toJS(this.store.unCheckedRows))
    }

    @computed
    get rowsCount() {
        if (this.store.accordion) {
            return this.store.data.map((groupStore) => groupStore.rows.length).reduce((a, b) => a + b, 0)
        } else {
            return toJS(this.store.data)[0].rows.length
        }
    }

    @action
    reloadData(newConf) {
        if (newConf) {
            this.store.conf = newConf
        }

        this.store.header = [];
        this.page = 1;
        this._fetchTable(1);
    }

    @action fetch() {
        if (!(this.page + 1 > this.pages) && !this.isFetching) {
            this._fetchTable(this.page + 1);
        }
    }

    stopRefresh() {
        if (!this.store.autoRefresh) {
            return
        }

        if (this.store.refreshIntervalId) {
            clearInterval(this.store.refreshIntervalId);
            this.store.refreshIntervalId = undefined;
        }
    }

    startRefresh() {
        if (!this.store.autoRefresh) {
            return
        }

        if (!this.store.refreshIntervalId) {
            this.store.refreshIntervalId = setInterval(() => {
                if (this.page > 0) {
                    this.fetchByPage(this.page)
                }
            }, this.store.autoRefresh * 1000)
        }
    }
}

class GridStore {
    @observable data;
    @observable header;
    @observable footer;

    @observable checkedRows = observable.map({});
    @observable unCheckedRows = observable.map({});


    constructor(conf) {
        this.conf = conf || {};
        this.conf.request.params = this.conf.request.params || {};

        this.autoRefresh = this.conf.autoRefresh // seconds
        this.refreshIntervalId = undefined;

        // init
        this.init = this.conf.init !== false;

        // Аккордион таблица с разбивкой на группы:
        // При accordion=True формат данных изменяется: см. action._fetchTable
        this.accordion = (conf.accordion === true);

        // pagination
        this.disableCount = this.conf.disableCount === true;
        this.disablePagination = this.conf.disablePagination === true;
        this.paginationCallback = this.conf.paginationCallback;

        this.checkMulti = (conf.checkMulti !== false);

        this.storage = BaseStorage;
        this.cellConstructors = {...tableCellConstructors, ...conf.cellConstructors || {}};
        this.fieldConstructors = {...tableFieldConstructors, ...conf.fieldConstructors || {}};

        this.data = [];
        this.fields = [];
        this.header = [];

        this.packCheckedRows(conf.checkedRows);

        this.checkCallback = this.conf.checkCallback; // checkbox выделение
        this.checkable = this.conf.checkable === true || this.checkCallback; // если колбек не нужен, а чек - да

        this.rowCallback = this.conf.rowCallback; // клик по строчке
        this.actions = new TableActions(this);
        this.ActionsComponent = conf.ActionsComponent;

        this.idKey = this.conf.idKey; // "id" by default
    }

    packCheckedRows(rows) {
        if (!rows) {
            return;
        }

        if (!Array.isArray(rows) && rows) {
            console.log("[WARNING] checked rows is not array: ", rows);
            return;
        }

        let checkedRows = {};
        rows.filter((el) => {
            if (el !== undefined) {
                checkedRows[el] = true
            }
        });

        this.checkedRows.merge(checkedRows);
    }
}

export {GridStore}
