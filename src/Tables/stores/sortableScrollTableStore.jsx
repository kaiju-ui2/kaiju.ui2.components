import BaseStorage from 'stores/storage'
import tableCellConstructors, {tableFieldConstructors} from "Tables/constructors";
import {action, computed, observable, runInAction, toJS} from "mobx";
import axios from 'axios';
import arrayMove from "array-move";
import {notifySuccess} from "notifications";
import {getTranslation} from "utils";
import {handleNotifyExceptions} from "utils/exceptions";

class SortableRowStore {
    @observable row = observable.map({});

    constructor(data, store) {
        this.data = data;
        this.row.merge(data);
        this.store = store;
    }

    @computed get id() {
        return Object.values(toJS(this.row))[0].id
    }

    get isSystem() {
        return Object.values(this.data)[0]["is_system"] === true;
    }

    get isDefault() {
        return Object.values(this.data)[0]["is_default"] === true;
    }

}


class SortableTableActions {
    @observable isFetching;
    @observable page;
    @observable pages;
    @observable count;

    constructor(store) {
        this.store = store;

        this.page = 0;
        this.pages = 1;
        this.isFetching = false;

        this.fetch()
    }

    updateSort(oldIndex, newIndex) {
        if (oldIndex === newIndex) {
            return
        }

        // first visual part - then request
        let oldData = this.store.data;
        let id = oldData[oldIndex].id;
        this.store.data = arrayMove(this.store.data, oldIndex, newIndex);

        if (!this.store.conf.requests.updateSort) {
            return
        }

        let params = {
            ...this.store.conf.requests.updateSort.params,
            sort: newIndex
        };
        params[this.store.idKey] = id;

        axios.post("/public/rpc",
            {
                "method": this.store.conf.requests.updateSort.method,
                "params": params
            }
        ).then(response => {
            if (response.data.result.updated) {
                notifySuccess()
            } else {
                this.store.data = oldData;
                handleNotifyExceptions(response);
            }
        }).catch(response => {
            this.store.data = oldData;
            handleNotifyExceptions(response)
        })
    }

    @action
    excludeId(id) {
        if (!this.store.excludedIds.includes(id)) {
            this.store.excludedIds.push(id);
            this.store.data = this.store.data.filter(store => store.id !== id);
        }
    }

    @action
    includeIdBack(currentId) {
        if (this.store.excludedIds.includes(currentId)) {
            this.store.excludedIds = this.store.excludedIds.filter(id => id !== currentId);
            this.store.data = this.store.data.filter(store => store.id !== currentId);
        }
    }

    @action
    fetchNext() {
        if (!(this.page + 1 > this.pages) && !this.isFetching) {
            this.fetch();
        }
    }

    @action
    fetchFirst() {
        this.page = 1;
        this.store.data = [];
        this._fetchTable(this.page)
    }

    @action
    _fetchTable(page) {
        this.isFetching = true;
        let params = this.store.conf.requests.load.params;

        axios.post("/public/rpc",
            {
                "method": this.store.conf.requests.load.method,
                "params": {
                    page: page,
                    ...params,
                }
            }
        ).then(response => {
            runInAction(() => {
                if (response.data.result) {
                    let data;
                    this.pages = response.data.result.pagination.pages;
                    this.page = response.data.result.pagination.page;
                    this.count = response.data.result.count;

                    let result = response.data.result;

                    let header = result["header"];

                    if (!this.store.header.length) {
                        if (header) {
                            this.store.header = header
                        } else {
                            this.store.header = result.fields.map(function (key) {
                                return {
                                    "key": key,
                                    "label": getTranslation(`Field.${key}`)
                                }
                            });
                        }
                    }

                    data = result.data.map((row) => (new SortableRowStore(row, this.store)));
                    let filteredData = data.filter(store => !this.store.excludedIds.includes(store.id));
                    this.store.data.push(...filteredData);

                    // Fetch one more page if data was filtered
                    if (filteredData.length < data) {
                        console.log("Fetch one more")
                        this._fetchTable(page + 1)
                    }
                }
                this.isFetching = false;
            })
        }).catch(response => {
            this.isFetching = false;
            handleNotifyExceptions(response)
        })
    }

    @action
    reloadData(newConf) {
        if (newConf) {
            this.store.conf = newConf
        }

        this.store.header = [];
        this.page = 1;
        this._fetchTable(1);
    }

    @action fetch() {
        if (!(this.page + 1 > this.pages) && !this.isFetching) {
            this._fetchTable(this.page + 1);
        }
    }

    stopRefresh() {
        if (!this.store.autoRefresh) {
            return
        }

        if (this.store.refreshIntervalId) {
            clearInterval(this.store.refreshIntervalId);
            this.store.refreshIntervalId = undefined;
        }
    }

    startRefresh() {
        if (!this.store.autoRefresh) {
            return
        }

        if (!this.store.refreshIntervalId) {
            this.store.refreshIntervalId = setInterval(() => {
                if (this.page > 0) {
                    this.fetchByPage(this.page)
                }
            }, this.store.autoRefresh * 1000)
        }
    }

}

class SortableScrollTableStore {
    @observable data;
    @observable header;

    constructor(conf) {
        this.conf = conf || {};
        this.disableCount = this.conf.disableCount === true;
        this.disableSort = this.conf.disableSort === true;
        this.idKey = this.conf.idKey; // "id" by default
        this.excludedIds = this.conf.excludedIds || [];

        this.storage = BaseStorage;
        this.cellConstructors = {...tableCellConstructors, ...conf.cellConstructors || {}};
        this.fieldConstructors = {...tableFieldConstructors, ...conf.fieldConstructors || {}};

        this.data = [];
        this.fields = [];
        this.header = [];

        this.rowCallback = this.conf.rowCallback; // клик по строчке
        this.actions = new SortableTableActions(this);
        this.ActionsComponent = conf.ActionsComponent
    }

}

export {SortableScrollTableStore}
