import {inject} from "mobx-react";
import React from "react";
import {uuid4} from "utils";

@inject("tableStore")
export default class CellComponent extends React.Component {


    renderCell(row, header) {
        // рендерим только те ключи, которые есть в хедере
        let value;
        const field = header["key"];
        let cell = row[field];


        if (cell) {
            const kind = cell["kind"];

            let cellConstructor = this.props.tableStore.fieldConstructors[field] ?
                this.props.tableStore.fieldConstructors[field] : this.props.tableStore.cellConstructors[kind];

            if (!cellConstructor) {
                cellConstructor = this.props.tableStore.cellConstructors["default"]
            }

            if (cellConstructor) {
                value = cellConstructor.bind(this)(cell, row);
            } else {
                value = cell.value ? cell.value : ""
            }
        }

        return (
            <td className="v-align-center" key={uuid4()}>
                {value}
            </td>
        )
    }

    render() {
        let {row, header} = this.props;
        return this.renderCell(row, header)
    }
}
