import {inject, observer} from "mobx-react";
import React from "react";
import {uuid4} from "utils";
import {toJS} from "mobx";
import PaginationComponent from "./Pagination";
import MediaCellComponent from "./MediaCellComponent";
import './PicturesGrid.scss'
import "./TableComponent.scss"

@inject("tableStore")
@observer
class GridBody extends React.Component {
    constructor(props) {
        super(props)
        
        this.paginationClassName = this.props.paginationClassName || "";
    }

    renderGrid(row) {
        return (
            <React.Fragment>
                {Object.values(row.data).map((el) => {
                    return (
                        el.kind == "media_file" && 
                        <MediaCellComponent    
                            key={uuid4()}
                            file={toJS(el)}
                            rowStore={toJS(row.data)}
                        />       
                    )
                })}
            </React.Fragment>
        )
    }

    render() {
        return (
            <>
                {
                    !this.props.tableStore.disablePagination &&
                    <PaginationComponent className={this.paginationClassName}/>
                }
                <div className="pictures-grid">
                {
                    this.props.tableStore.data.map((group) => {
                        return group.rows.map((row) => {
                            return (
                                <React.Fragment key={uuid4()}>
                                    {this.renderGrid(row)}
                                </React.Fragment>
                                )
                            })
                    })
                }
                </div>
            </>
        )
    }
}


@observer
export default class PicturesGrid extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            this.props.count > 0 ? <GridBody /> : (
                <p className="notice notice_empty">{this.props.emptyNotice}</p>
            )
        )
    }
}
