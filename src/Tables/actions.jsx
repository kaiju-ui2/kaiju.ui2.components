import React from "react";
import {WindowAlertComponent, WindowDeleteComponent} from "Window";
import {inject} from "mobx-react";

class ActionIcon extends React.Component {
    iconClass;

    // callback - без модального окна (!) autorefresh продолжит работать
    // deleteCallback - с модальным окном удаления - авторефреш остановится
    // confirmCallback - с модальным окном подтверждения - авторефреш остановится

    constructor(props) {
        super(props);
        this.iconClass = this.props.iconClass || this.iconClass;
        this.tableStore = this.props.tableStore;
    }

    render() {
        return (
            <React.Fragment>
                {
                    this.props.callback &&
                    <i onClick={() => this.props.callback()}
                       className={`${this.iconClass} pointer pl-3`}
                       style={{"fontSize": "18px"}}/>
                }
                {
                    this.props.deleteCallback &&
                    <WindowDeleteComponent
                        idLabel={this.props.idLabel}
                        actionLabel={this.props.actionLabel}
                        startCallback={() => {
                            this.tableStore ? this.tableStore.actions.stopRefresh() : null
                        }}
                        actionCallback={() => {
                            this.props.deleteCallback();
                            this.tableStore ? this.tableStore.actions.startRefresh() : null
                        }}
                        cancelCallback={() => {
                            this.tableStore ? this.tableStore.actions.startRefresh() : null
                        }}
                    >
                        <i
                            className={`${this.iconClass} pointer pl-3`}
                            style={{"fontSize": "18px"}}/>
                    </WindowDeleteComponent>
                }

                {
                    this.props.confirmCallback &&
                    <WindowAlertComponent
                        idLabel={this.props.idLabel}
                        actionLabel={this.props.actionLabel}
                        startCallback={() => {
                            this.tableStore ? this.tableStore.actions.stopRefresh() : null
                        }}
                        actionCallback={() => {
                            this.props.confirmCallback();
                            this.tableStore ? this.tableStore.actions.startRefresh() : null

                        }}>
                        cancelCallback={() => {
                        this.tableStore ? this.tableStore.actions.startRefresh() : null
                    }}
                        <i
                            className={`${this.iconClass}`}
                            style={{"fontSize": "18px"}}/>
                    </WindowAlertComponent>
                }

            </React.Fragment>
        )
    }
}

@inject("tableStore")
class DeleteAction extends ActionIcon {
    iconClass = "icon-delete";
}

@inject("tableStore")
class EditAction extends ActionIcon {
    iconClass = "icon-edit";
}

@inject("tableStore")
class ViewAction extends ActionIcon {
    iconClass = "icon-view";
}

export default class ActionsComponent extends React.Component {
    // использовать для сборки экшенов
    render() {
        return (
            <div className="action-block hide">
                {this.props.children}

                {/* Пример: */}
                {/*<DeleteAction deleteCallback={() => console.log("delete")}/>*/}
                {/*<DeleteAction confirmCallback={() => console.log("delete")}/>*/}
                {/*<DeleteAction callback={() => console.log("delete")}/>*/}
            </div>
        )
    }

}

export {DeleteAction, EditAction, ViewAction, ActionsComponent, ActionIcon}