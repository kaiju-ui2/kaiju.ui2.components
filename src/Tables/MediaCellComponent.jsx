import {inject} from "mobx-react";
import React from "react";
import {uuid4} from "utils";

@inject("tableStore")
export default class MediaCellComponent extends React.Component {
    renderCell(cell, fileName) {
        return (
            <div 
                className="cell"  
                onClick={() => {
                    this.props.tableStore.rowCallback &&
                    this.props.tableStore.rowCallback(cell)
                }} 
            >
                <div className="cell__image-wrapper">
                    <img src={cell.value.original.path} alt=""/>
                </div>
                <p>{fileName}</p>
            </div>
        )
    }

    render() {
        const cell = this.props.file

        if (cell) {
            return this.renderCell(cell, this.props.rowStore.label.value)
        }
    }
}
