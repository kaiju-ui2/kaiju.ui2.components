export default class BaseStorage {
    static setItem(key, value) {
        window.sessionStorage.setItem(key, JSON.stringify(value))
    }

    static getItem(key, def) {
        let value = window.sessionStorage.getItem(key);
        try {
            value = JSON.parse(value)
        } catch (e) {
            value = def
        }

        return value !== undefined ? value : def
    }

    static removeItem(key) {
        window.sessionStorage.removeItem(key)
    }

    static removeAllStartWith (string) {
        let keys = Object.keys(window.sessionStorage);
        for(let key of keys) {
            if (key.split('.')[0] === string) {
                BaseStorage.removeItem(key);
            }
        }
    }
}
