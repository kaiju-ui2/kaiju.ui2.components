import React from 'react';
import "./Buttons.scss"
import {uuid4} from "utils";
import {action, observable} from "mobx";
import {observer} from "mobx-react";
import Icon from "../Icon";

class Button extends React.Component {
    defaultClassName = "btn";

    render() {
        let {
            onClick = undefined,
            callback = undefined,
            className,
            children,
            type = "button",
            disabled = false,
            style = {},
            icon,
            label,
            onKeyPress = false,
        } = this.props;

        return (
            <button
                style={style}
                disabled={disabled}
                className={`${this.defaultClassName} ${className}`}
                type={type}
                onClick={onClick || callback}

                // TODO: onKeyPress={onKeyPress ? onKeyPressFunc(onClick || callback) : undefined}
            >
                {

                    icon &&
                    <Icon id={icon} fill='#fff' className={`${icon} pr-3`}/>
                }

                {
                    children &&
                    children
                }

                {
                    label &&
                    label
                }
            </button>
        )
    }
}

class ButtonSuccess extends React.Component {
    classNames = "m-btn--pill btn-success";

    render() {

        return (
            <Button
                {...this.props}
                className={`${this.classNames} ${this.props.className || ""}`}
            />
        )

    }
}

class ButtonDefault extends React.Component {
    classNames = "m-btn--pill btn-default";

    render() {

        return (
            <Button
                {...this.props}
                className={`${this.classNames} ${this.props.className || ""}`}
            />
        )

    }
}

/*
Simple dropdown with props children
 */
class SimpleButtonDropdown extends React.Component {

    state = {
        show: false
    };
    dropdownRef;
    buttonRef;

    constructor(props) {
        super(props);

        this.state = {
            show: false
        };
    }

    componentDidMount() {
        document.addEventListener('mouseup', (e) => this.handleClickOutside(e));
    }

    handleClickOutside(event) {
        if (!this.state.show) {
            return
        }

        if (this.buttonRef && this.buttonRef.contains(event.target)) {
            return
        }

        if (this.dropdownRef && !this.dropdownRef.contains(event.target)) {
            this.setState({show: false})
        }
    }

    callback() {
        this.props.conf.callback;
    }

    render() {
        const buttonConf = {
            callback: () => this.setState({show: !this.state.show}),
            label: this.props.label,
            icon: this.props.icon,
            className: this.props.buttonClass
        };

        return (
            <React.Fragment>
                <div>
                    <div
                        className="btn-dropdown"
                        ref={ref => this.buttonRef = ref}>
                        <ButtonDefault {...buttonConf}/>
                    </div>
                    {this.state.show &&
                    <div
                        ref={ref => this.dropdownRef = ref} className="show btn-dropdown-content">
                        {this.props.children}
                    </div>}
                </div>
            </React.Fragment>
        )
    }
}


/*
  Config Example which shoud be passed to props
  conf = {
            mainButtonConf: {
                label: utils.getTranslation("Save and next"),
                callback: () => {
                    console.log("Save")
                }
            },
            (dropDownConfs OR items): [
                {
                    label: "Save",
                    callback: () => {
                        console.log("test")
                    }
                },
            ]
        };
 */
class DropdownBase extends React.Component {
    @observable show = false;
    @observable dropdownOffset = 0;

    dropdownRef;
    buttonRef;


    componentDidMount() {
        document.addEventListener('mouseup', (e) => this.handleClickOutside(e));
    }

    handleClickOutside(event) {
        if (!this.show) {
            return
        }

        if (this.buttonRef && this.buttonRef.contains(event.target)) {
            return
        }

        if (this.dropdownRef && !this.dropdownRef.contains(event.target)) {
            this.toggleShow(false)
        }
    }

    onClickCallback(callback) {
        callback();
        this.toggleShow()
    }

    @action
    toggleShow(value) {
        if (typeof value == "boolean") {
            this.show = value;
            return
        }

        this.show = !this.show
    }

    @action
    calcDropdownOffset() {
        if (!this.dropdownRef || !this.buttonRef) {
            return 0
        }

        // если дропдаун по ширине меньше чем 1.2 кнопки, то выравниваться будет по левому краю
        if (this.dropdownRef.offsetWidth < this.buttonRef.offsetWidth * 1.2) {
            return
        }

        // если нет - выравнивание будет по правому краю
        let newOffset = this.buttonRef.offsetWidth - this.dropdownRef.offsetWidth

        if (newOffset < this.dropdownOffset) {
            this.dropdownOffset = newOffset
        }
    }


    renderDropdown = observer(({items}) => {
        return (
            <div ref={ref => {
                this.dropdownRef = ref;
                this.calcDropdownOffset()
            }}
                 className={`${this.show ? "show" : "hide"} btn-dropdown__select`}
                 style={{left: this.dropdownOffset}}>
                {items.map(conf =>
                    <a key={uuid4()}
                       onClick={() => this.onClickCallback(conf.callback)}
                       className="btn-dropdown__item pointer">{conf.label}</a>
                )}
            </div>
        )
    })
}


class DoubleButtonDropdown extends DropdownBase {

    constructor(props) {
        super(props);
    }

    render() {
        const {mainButtonConf, dropDownConfs} = this.props.conf;
        let iconClass = mainButtonConf.iconClass || "icon-down-arrow";
        return (
            <React.Fragment>
                <div>
                    <div
                        className="btn-dropdown"
                        ref={ref => this.buttonRef = ref}>
                        <ButtonSuccess

                            {...mainButtonConf}
                            className="btn-dropdown-first border-right"/>
                        <ButtonSuccess
                            callback={() => this.toggleShow()}
                            className="btn-dropdown-last">
                            <i className={`${iconClass}`} style={{fontSize: '17px'}}/>
                        </ButtonSuccess>
                    </div>
                    <this.renderDropdown items={dropDownConfs}/>
                </div>
            </React.Fragment>
        )
    }
}

/*
dropdown with items
 */
class ButtonDropdown extends DropdownBase {

    constructor(props) {
        super(props);
        this.buttonConf = {
            callback: () => this.toggleShow(),
        };
    }

    render() {
        return (
            <React.Fragment>
                <div>
                    <div
                        className="btn-dropdown"
                        ref={ref => this.buttonRef = ref}>
                        <ButtonSuccess {...this.buttonConf}>
                            {this.props.conf.label}
                            <i className={"icon-down-arrow pl-3"} style={{fontSize: '15px'}}/>
                        </ButtonSuccess>
                    </div>
                    <this.renderDropdown items={this.props.conf.items}/>
                </div>
            </React.Fragment>
        )
    }
}

export {ButtonSuccess, ButtonDefault, Button, DoubleButtonDropdown, ButtonDropdown, SimpleButtonDropdown}