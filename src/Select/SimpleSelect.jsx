import React from "react";
import Select, {components} from 'react-select';
import {customStyles, DropDownIcon, LoadingMessage, NoOptions} from "./common";


/* Simple select with options
{
options : [
  { value: 'vanilla', label: 'Vanilla', rating: 'safe' },
  { value: 'chocolate', label: 'Chocolate', rating: 'good' },
  { value: 'strawberry', label: 'Strawberry', rating: 'wild' },
  { value: 'salted-caramel', label: 'Salted Caramel', rating: 'crazy' },
];
defaultValue : [{ value: 'vanilla', label: 'Vanilla', rating: 'safe' }]
onChange : (option) => console.log(option)
// { value: 'vanilla', label: 'Vanilla', rating: 'safe' }
}
 */
export class SimpleSelectComponent extends React.Component {
    constructor(props) {
        super(props);
        this.conf = this.props.conf;

        // убираем часть компонентов
        this.selectComponents = {
            SingleValue: this.makeSingleValueComponent(),
            DropdownIndicator: null,
            indicatorSeparator: null,
            LoadingIndicator: null,
            LoadingMessage: LoadingMessage,
            NoOptionsMessage: NoOptions,
            ...this.conf.selectComponents
        }
    }

    makeSingleValueComponent() {
        // Новый компонент одиночного значения без бордеров и тд.

        return ({children, ...props}) => {
            return (
                <React.Fragment>
                    {
                        this.conf.onlyIcon &&
                        <components.SingleValue {...props}>
                            {this.conf.icon || <DropDownIcon {...this.props}/>}
                        </components.SingleValue>
                    }

                    {
                        !this.conf.onlyIcon &&
                        <components.SingleValue {...props}>
                            {
                                this.conf.label &&
                                <span style={{fontWeight: 600}}>{this.conf.label}:</span>
                            }
                            {children}

                            {this.conf.icon || <DropDownIcon {...this.props}/>}
                        </components.SingleValue>
                    }
                </React.Fragment>
            )
        };
    }

    render() {
        return (
            <Select
                defaultValue={this.conf.defaultValue}
                options={this.conf.options}

                styles={this.conf.style || customStyles}
                components={this.selectComponents}
                isClearable={false}
                isSearchable={false}
                // isDisabled={this.store.isFetching}
                // isMulti={this.props.conf.isMulti || false}
                onChange={(vals, actions, e) => this.conf.onChange(vals, actions, e)}
                // closeMenuOnSelect={!(this.props.conf.isMulti || false)}
                placeholder={""}

                menuPlacement={this.props.conf.menuPlacement || "auto"}

            />
        )
    }
}