import {observer} from "mobx-react";
import React from "react";
import {action, computed, observable} from "mobx";
import CheckboxComponent from "Checkbox";
import ESelectStore from "./store";
import {components} from "react-select";
import {AsyncPaginate} from "react-select-async-paginate";
import "./SelectComponent.scss";
import {LoadingMessage, NoOptions} from "./common";
import axios from "axios";
import BaseStorage from "stores/storage";

const customStyles = {
    menu: (provided) => {
        return {
            ...provided,
            marginTop: 0,
            borderRadius: "0 0 4px 4px"
        }
    },
    container: (provided) => {
        return {
            ...provided,
            position: "absolute",
            marginTop: "8px"
        }
    },
    control: (provided) => {
        return {
            // ...provided,
            border: "none",
        }
    },
    input: (provided) => {
        return {
            ...provided,
            paddingLeft: "40px"
        }
    },
    placeholder: (provided) => {
        return {
            ...provided,
            paddingLeft: "40px",
            fontWeight: "400"
        }
    }
};

class ChoiceSelectStore extends ESelectStore {
    @observable menuIsOpen = false;
    @observable inputValue = "";
    @observable checkedElements = [];
    @observable isNull = false;

    constructor(props) {
        super(props);
        this.conf = props.conf || {};
        this.onChange = props.onChange;
        this.enableNull = this.conf.enableNull === true;
    }

    @action.bound onMenuOpen() {
        this.setMenuOpen(!this.menuIsOpen)
    }

    @action.bound onInputChange(value) {
        this.inputValue = value
    }

    @action.bound setMenuOpen(state) {
        this.menuIsOpen = state
    }

    @computed get isAll() {
        if (this.isNull === true) {
            return false
        }

        return this.checkedElements.length === 0;
    }

    @computed get label() {
        if (this.isNull) {
            return utils.getTranslation("Select.null")
        }

        let label = this.checkedElements.map(el => {
            return el.label
        }).join(", ");

        return label.length > 0 ? label : utils.getTranslation("all")
    }

    @action.bound check(data) {
        this.setNull(false);

        if (this.checkedElements.some(el => {
            return el.value === data.value
        })) {
            this.checkedElements = this.checkedElements.filter(e => {
                return e.value !== data.value
            })
        } else {
            this.checkedElements.push(data)
        }
        this.onChange(this.checkedElements.map(el => (el.value)))
    }

    @action
    async preLoadOptions() {
        let id = Array.isArray(this.conf.value) ? this.conf.value : [this.conf.value];

        if (id.length > 0) {
            this.isFetching = true;

            const params = {
                key: this.conf.key,
                page: 1,
                id: id,
                locale: BaseStorage.getItem("locale"),
                ...this.conf.extraParams,
                ...this.conf.params,
            };

            let response = await axios.post("/public/rpc",
                {
                    method: this.conf.options_handler,
                    params: params
                }
            );

            if (response.data.result) {
                let newVal = [];

                if (response.data.result.data.length > 0) {
                    let respIds = response.data.result.data.map((val) => val.id)

                    response.data.result.data.map((val) => {
                        newVal.push({
                            id: val.id,
                            value: val.id,
                            label: val.label,
                        })
                    });

                    id.map((v) => {
                        if (!respIds.includes(v)) {
                            newVal.push({
                                id: val.id,
                                value: v,
                                label: v,
                            })
                        }
                    })

                } else {
                    newVal = id.map((val) => {
                        return {value: val, label: val}
                    })
                }
                this.value = newVal;
                console.log("NEW VALU", newVal)
                this.checkedElements.push(...newVal)
            }
            this.isFetching = false;

        }
    }

    @computed get count() {
        return this.checkedElements.length
    }

    checked(data) {
        return this.checkedElements.some(el => {
            return el.value === data.value
        })
    }

    @action.bound setAll() {
        this.checkedElements = [];
        this.setNull(false);
        this.onChange([]);
    }

    @action.bound setNull(value) {
        this.isNull = value;
    }

    @action.bound nullOnChange(value) {
        this.setNull(value);
        this.checkedElements = [];
        this.onChange(value === true ? null : [])
    }

}

@observer
export default class CheckboxSelectComponent extends React.Component {
    constructor(props) {
        super(props);
        this.store = new ChoiceSelectStore({conf: this.props.conf, onChange: props.onChange});
        this.dropdownRef = undefined;
        this.buttonRef = undefined;


        // убираем часть компонентов
        this.selectComponents = {
            DropdownIndicator: null,
            indicatorSeparator: null,
            LoadingIndicator: null,
            Option: this.CustomOption,
            Menu: this.Menu,
            MenuList: this.MenuList,
            Control: this.Control,
            IndicatorsContainer: this.IndicatorContainer,
            LoadingMessage: LoadingMessage,
            NoOptionsMessage: NoOptions
        };

        // if (this.store.conf.disableSearch) {
        // TODO:
        //     this.selectComponents["Control"] = null
        // }
    }

    Input = props => {
        return (
            <components.Control {...props} className="select-dropdown__input"/>
        )
    };

    MenuList = props => {
        return (
            <components.MenuList {...props} className="select-dropdown__menu-list">
                <this.AllCheckBox/>
                {
                    this.store.enableNull &&  <this.NullCheckBox/>
                }
                {props.children}
            </components.MenuList>)
    };

    AllCheckBox = observer(() => {
        return <div className="pt-3 pl-3">
            {this.store.isAll &&
            <CheckboxComponent className={"select-color"} disabled={true}
                               defaultChecked={true} label={utils.getTranslation("all")}/>
            }
            {!this.store.isAll &&
            <CheckboxComponent defaultChecked={false} onChange={checked => {
                this.store.setAll()
            }} label={utils.getTranslation("all")}/>
            }
        </div>
    });

    NullCheckBox = observer(() => {
        return <div className="pt-3 pl-3">
            {this.store.isNull &&
            <CheckboxComponent defaultChecked={true} onChange={checked => {
                this.store.nullOnChange(false)
            }} label={utils.getTranslation("Select.null")}/>
            }
            {!this.store.isNull &&
            <CheckboxComponent defaultChecked={false} onChange={checked => {
                this.store.nullOnChange(true)
            }} label={utils.getTranslation("Select.null")}/>
            }
        </div>
    });


    IndicatorContainer = props => {
        return (
            <div style={{
                position: "absolute",
                top: "20px",
                left: "20px",
            }}>
                <i className="icon-search pr-3 pointer" style={{position: "relative", fontSize: "1.2rem", top: "3px"}}/>
            </div>
        )
    };

    Control = props => {
        return (
            <components.Control {...props} className="select-dropdown__search"/>
        )
    };

    CustomOption = props => {
        return (
            <this.CustomCheckbox {...props}/>
        )
    };

    CustomCheckbox = observer((props) => {
        return (
            <div className="pt-3 pl-3" ref={props.innerRef} {...props.innerProps}>
                {(this.store.isAll || this.store.isNull) &&
                <CheckboxComponent defaultChecked={false} onChange={checked => {
                    this.store.check(props.data)
                }} label={props.data.label}/>
                }
                {!(this.store.isAll || this.store.isNull) &&
                <CheckboxComponent defaultChecked={this.store.checked(props)} onChange={checked => {
                    this.store.check(props.data)
                }} label={props.data.label}/>
                }

            </div>)
    });


    Menu = props => {
        return (
            <components.Menu {...props} className="select-dropdown__menu">
                {props.children}
                <div className="p-2 text-center border-top select-dropdown__button">
                    <span className="pr-3">{utils.getTranslation("Count")}: <this.Count/></span>
                </div>
            </components.Menu>
        )
    };

    Label = observer(() => {
        return <React.Fragment>{this.store.label}</React.Fragment>
    });

    Count = observer(() => {
        return <React.Fragment>{this.store.count}</React.Fragment>
    });

    componentDidMount() {
        document.addEventListener('mouseup', (e) => this.handleClickOutside(e));
    }

    handleClickOutside(event) {
        if (!this.store.menuIsOpen) {
            return
        }

        if (this.buttonRef && this.buttonRef.contains(event.target)) {
            return
        }

        if (this.dropdownRef && !this.dropdownRef.contains(event.target)) {
            this.store.setMenuOpen(false)
        }
    }

    openDropdown(e) {
        e.stopPropagation();
        this.store.onMenuOpen()
    }

    render() {
        return (
            <div className="d-inline-block">

                <div className={`pointer ${this.props.className || ""}`}
                     ref={ref => this.buttonRef = ref}
                     onClick={e => {
                         this.openDropdown(e)
                     }}
                     style={{fontWeight: 400}}
                >
                    <span style={{fontWeight: 500}}>{this.store.conf.label}: </span>
                    <this.Label/>
                    <i className="icon-collapse select-dropdown__icon"/>

                </div>

                {this.store.menuIsOpen &&
                <span ref={ref => this.dropdownRef = ref}>
                    <AsyncPaginate
                        className={this.props.dropdownClassName || ""}
                        inputValue={this.store.inputValue}
                        onInputChange={this.store.onInputChange}
                        loadOptions={this.store.loadOptions}
                        defaultOptions
                        debounceTimeout={this.store.debounceTimeout}
                        components={this.selectComponents}
                        autoFocus
                        hideSelectedOptions={false}
                        controlShouldRenderValue={false}
                        backspaceRemovesValue={true}
                        tabSelectsValue={false}
                        isClearable={false}

                        menuIsOpen
                        styles={customStyles}

                        placeholder={utils.getTranslation("Placeholder.search")}
                        shouldLoadMore={this.store.shouldLoadMore}
                        closeMenuOnSelect={false}

                        value={""}
                        additional={{
                            page: 1,
                        }}
                    />
                </span>}
            </div>
        )
    }
}

