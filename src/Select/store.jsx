/*
* Перенёс из forms/Fields/Select
*
* */
import {action, computed, observable, toJS} from "mobx";
import axios from "axios";
import BaseStorage from "stores/storage";

export default class ESelectStore {
    @observable value;
    @observable isFetching;
    debounceTimeout = 200;

    constructor({conf, onChange}) {
        this.conf = conf;
        this.onChange = onChange;

        if (this.conf.value) {
            this.preLoadOptions()
        }
    }

    @computed get getValue() {
        console.log(toJS(this.value))
        return toJS(this.value)
    }

    // Not required. Function. By default new options will load only after scroll menu to bottom. Arguments:
    shouldLoadMore(scrollHeight, clientHeight, scrollTop) {
        const bottomBorder = (scrollHeight - clientHeight) + 50;
        return bottomBorder < scrollTop;
    }

    @action.bound
    async loadOptions(search, loadedOptions, additional) {
        if (this.conf.options_handler) {
            let hasMore = false, options = []

            const params = {
                key: this.conf.key,
                page: additional.page,
                locale: BaseStorage.getItem("locale"),
                ...this.conf.extraParams,
                ...this.conf.params,
            };

            if (search && search.length > 0) {
                params.query = search
            }

            let response = await axios.post("/public/rpc",
                {
                    method: this.conf.options_handler,
                    params: params
                }
            );

            if (response.data.result) {
                hasMore = additional.page < response.data.result.pagination.pages;
                options = response.data.result.data.map((current) => {
                    current.value = current.id
                    return current
                })
            }


            return {
                options: options,
                hasMore: hasMore,
                additional: {
                    page: additional.page + 1,
                },
            }
        } else {
            return {
                options: [],
                hasMore: false,
                additional: {
                    page: 1,
                },
            }
        }
    }

    @action
    async preLoadOptions() {
        let id = Array.isArray(this.conf.value) ? this.conf.value : [this.conf.value];
        
        if (id.length > 0 && this.conf.options_handler) {
            this.isFetching = true;

            const params = {
                key: this.conf.key,
                page: 1,
                id: id,
                locale: BaseStorage.getItem("locale"),
                ...this.conf.extraParams,
                ...this.conf.params,
            };

            let response = await axios.post('/public/rpc',
                {
                    method: this.conf.options_handler,
                    params: params
                }
            );
            if (response.data.result) {
                if (this.conf.isMulti) {
                    let newVal = [];

                    if (response.data.result.data.length > 0) {
                        let respIds = response.data.result.data.map((val) => val.id)

                        response.data.result.data.map((val) => {
                            newVal.push({
                                value: val.id,
                                label: val.label,
                            })
                        });

                        id.map((v) => {
                            if (!respIds.includes(v)) {
                                newVal.push({
                                    value: v,
                                    label: v,
                                })
                            }
                        })

                    } else {
                        newVal = id.map((val) => {
                            return {value: val, label: val}
                        })
                    }
                    this.value = newVal;

                } else {
                    if (response.data.result.data.length > 0) {
                        let data = response.data.result.data[0];
                        this.value = {
                            value: data.id,
                            label: data.label
                        }
                    } else {
                        this.value = {
                            value: this.conf.value,
                            label: this.conf.value
                        }
                    }
                }
            } else {

            }
            this.isFetching = false;

        } else if (this.conf.isCreatable) {
            const newVal = [];

            id.map((val) => {
                newVal.push({
                    value: val,
                    label: val,
                })
            });
            this.value = newVal;
        }
    }

    async fetch() {

        const params = {
            key: this.key,
            page: this.page,
            locale: BaseStorage.getItem("locale"),
            ...this.params
        };

        let response = await axios.post('/public/rpc',
            {
                method: this.conf.options_handler,
                params: params
            }
        )

    }

    @action addValue(values, actions) {
        if (actions.action === 'remove-value') {
            if (this.conf.isMulti) {
                let removedValue = actions.removedValue.value;
                this.value = this.value.filter(el => {
                    return removedValue !== el.value
                })
            }
        } else {
            this.value = values
        }

        let val = this.value;
        if (this.conf.isMulti) {
            val = Array.isArray(this.value) ? this.value.map((c) => (toJS(c.value))) : []
        } else {
            val = val ? val.value : val
        }

        // toJS(this.value) вся инфа по выбранному элементам(ту)
        this.onChange(val, toJS(this.value));
    }

}