import {observer} from "mobx-react";
import React from "react";
import {action, computed, observable, toJS} from "mobx";
import CheckboxComponent from "Checkbox";
import ESelectStore from "./store";
import {components} from "react-select";
import {ButtonSuccess} from "Buttons";
import {AsyncPaginate} from "react-select-async-paginate";
import "./SelectComponent.scss";
import {LoadingMessage, NoOptions} from "./common";

const customStyles = {
    menu: (provided) => {
        return {
            ...provided,
            marginTop: 0,
            borderRadius: "0 0 4px 4px"
        }
    },
    container: (provided) => {
        return {
            ...provided,
            position: "absolute",
            marginTop: "8px"
        }
    },
    control: (provided) => {
        return {
            // ...provided,
            border: "none",
        }
    },
    input: (provided) => {
        return {
            ...provided,
            paddingLeft: "40px"
        }
    },
    placeholder: (provided) => {
        return {
            ...provided,
            paddingLeft: "40px",
            fontWeight: "400"
        }
    }
};

class ChoiceSelectStore extends ESelectStore {
    @observable menuIsOpen = false;
    @observable inputValue = "";
    @observable checkedElements = [];

    constructor(props) {
        super(props);
        this.onChange = props.onChange;
        this.disableSearch = this.conf.disableSearch === true;
    }

    @action.bound onMenuOpen() {
        this.checkedElements = [];
        this.setMenuOpen(!this.menuIsOpen)
    }

    @action.bound onInputChange(value) {
        this.inputValue = value
    }

    @action.bound setMenuOpen(state) {
        this.menuIsOpen = state
    }

    @action.bound add() {
        this.setMenuOpen(false);
        this.onChange(toJS(this.checkedElements))
        this.checkedElements = [];
    }

    @action.bound check(val) {
        if (this.checkedElements.includes(val)) {
            this.checkedElements = this.checkedElements.filter(e => {
                return e !== val
            })
        } else {
            this.checkedElements.push(val)
        }
    }

    @computed get count() {
        return this.checkedElements.length
    }
}

@observer
export default class AddCheckboxSelectComponent extends React.Component {
    constructor(props) {
        super(props);
        this.store = new ChoiceSelectStore({conf: this.props.conf, onChange: props.onChange})
        this.dropdownRef = undefined;
        this.buttonRef = undefined;

        // убираем часть компонентов
        this.selectComponents = {
            DropdownIndicator: null,
            indicatorSeparator: null,
            LoadingIndicator: null,
            Option: this.CustomOption,
            Menu: this.Menu,
            MenuList: this.MenuList,
            Control: this.Control,
            IndicatorsContainer: this.IndicatorContainer,
            LoadingMessage: LoadingMessage,
            NoOptionsMessage: NoOptions
        }
    }

    Input = props => {
        return (
            <components.Control {...props} className="select-dropdown__input"/>
        )
    };

    IndicatorContainer = props => {
        return (
            <div style={{
                position: "absolute",
                top: "20px",
                left: "20px",
            }}>
                <i className="icon-search pr-3 pointer" style={{position: "relative", fontSize: "1.2rem", top: "3px"}}/>
            </div>
        )
    };

    Control = props => {
        return (
            <components.Control {...props} className="select-dropdown__search"/>
        )
    };

    CustomOption = props => {
        const {data, innerRef, innerProps} = props;
        return (
            <div className="pt-3 pl-3 pr-3" ref={innerRef} {...innerProps}>
                <CheckboxComponent onChange={checked => {
                    this.store.check(data.value)
                }} label={data.label}/>
            </div>)
    };

    Menu = props => {
        let conf = {
            label: utils.getTranslation("Button.add"),
            onClick: () => {
                this.store.add()
            }
        };
        return (
            <components.Menu {...props} className="select-dropdown__menu">
                {props.children}
                <div className="p-2 text-center border-top select-dropdown__button">
                    <span className="pr-3">{utils.getTranslation("Count")}: <this.Count/></span>
                    <ButtonSuccess {...conf}/>
                </div>
            </components.Menu>
        )
    };
    Count = observer(() => {
        return <React.Fragment>{this.store.count}</React.Fragment>
    });

    MenuList = props => {
        return <components.MenuList {...props} className="select-dropdown__menu-list"/>
    };

    componentDidMount() {
        document.addEventListener('mouseup', (e) => this.handleClickOutside(e));
    }

    handleClickOutside(event) {
        if (!this.store.menuIsOpen) {
            return
        }

        if (this.buttonRef && this.buttonRef.contains(event.target)) {
            return
        }

        if (this.dropdownRef && !this.dropdownRef.contains(event.target)) {
            this.store.setMenuOpen(false)
        }
    }

    openDropdown(e) {
        e.stopPropagation();
        this.store.onMenuOpen()
    }

    render() {
        return (
            <div className="d-inline-block">

                <span
                    ref={ref => this.buttonRef = ref}
                    className={`badge badge-pill badge-info p-3 fs-12 pointer no-select ${this.props.className || ""}`}
                    style={{fontWeight: 500, fontSize: "1rem"}}
                    onClick={(e) => this.openDropdown(e)}>
                    {this.store.conf.label}
                    <i className="icon-collapse select-dropdown__icon"/>
                </span>

                {this.store.menuIsOpen &&
                <span ref={ref => this.dropdownRef = ref}>
                    <AsyncPaginate
                        className={this.props.dropdownClassName || ""}
                        inputValue={this.store.inputValue}
                        onInputChange={this.store.onInputChange}
                        loadOptions={this.store.loadOptions}
                        defaultOptions
                        debounceTimeout={this.store.debounceTimeout}
                        components={this.selectComponents}
                        autoFocus
                        hideSelectedOptions={false}
                        controlShouldRenderValue={false}
                        backspaceRemovesValue={true}
                        tabSelectsValue={false}
                        isClearable={false}

                        menuIsOpen
                        styles={customStyles}

                        placeholder={utils.getTranslation("Placeholder.search")}
                        // onMenuOpen={e => this.store.onMenuOpen()}
                        shouldLoadMore={this.store.shouldLoadMore}
                        closeMenuOnSelect={false}

                        value={""}
                        additional={{
                            page: 1,
                        }}
                    />
                </span>}


            </div>


        )
    }
}

