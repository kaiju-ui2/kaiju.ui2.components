import {observer} from "mobx-react";
import React from "react";
import {AsyncPaginate, withAsyncPaginate} from "react-select-async-paginate";
import CreatableSelect from 'react-select/creatable';
import ESelectStore from "./store";
import {LoadingMessage, NoOptions} from "./common";
import "./ESelectComponent.scss"

export default class ESelectComponent extends React.Component {
    constructor(props) {
        super(props);

        this.store = new ESelectStore({conf: this.props.conf, onChange: props.onChange})
    }
    
    reRender = observer(()=>{
        const SelectComponent = !this.props.conf.isCreatable
            ? AsyncPaginate
            : withAsyncPaginate(CreatableSelect);

        const handleChange = (vals, actions, e) => {
            this.store.addValue(vals, actions, e)
        }

        const handleCreate = (inputValue) => {
            const action = {
                action: 'create-option',
                name: 'create-option',
            }
            let vals = this.store.getValue
            let tagsArray = []
            const pushValues = (tags) => {
                inputValue.split(' ').forEach((el) => {
                    el.length && tags.push({
                        value: el,
                        label: el,
                    })
                })
            }
            if (vals == undefined) {
                pushValues(tagsArray)
                handleChange(tagsArray, action, undefined)
            } else {
                pushValues(vals)
                handleChange(vals, action, undefined)
            }
        }

        const handleKeyDown = (event) => {
            if (event.keyCode == 32) {
                handleCreate(event.target.value)
            }
        }

        const openProps = this.props.conf.menuIsOpen && {
            menuIsOpen: true,
        }

        return (
            <SelectComponent
                value={this.store.getValue}
                components={{
                    LoadingMessage: LoadingMessage,
                    NoOptionsMessage: NoOptions
                }}
                loadOptions={this.store.loadOptions}
                defaultOptions
                debounceTimeout={this.store.debounceTimeout}
                // cacheUniq
                isDisabled={this.props.conf.disabled === true || this.store.isFetching}
                isClearable={this.props.conf.isClearable !== false}
                isMulti={this.props.conf.isMulti || false}
                // shouldLoadMore={this.store.shouldLoadMore}
                onChange={handleChange}
                onKeyDown={(this.props.conf.isCreatable && this.props.conf.split_spaces) && handleKeyDown}
                onCreateOption={(this.props.conf.isCreatable && this.props.conf.split_spaces) && handleCreate}
                closeMenuOnSelect={!(this.props.conf.isMulti || false)}
                placeholder={""}
                additional={{
                    page: 1,
                }}
                {...openProps}

            />
        )
    });

    render() {
        return <this.reRender />
    }
}
