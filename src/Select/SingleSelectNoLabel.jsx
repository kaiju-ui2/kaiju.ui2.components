import {observer} from "mobx-react";
import React from "react";
import {AsyncPaginate} from "react-select-async-paginate";
import ESelectStore from "./store";
import {components} from 'react-select';
import {LoadingMessage, NoOptions} from "./common";


const customStyles = {
    control: (provided) => {
        // убрали все стили контрол компонента таким образов
        return {
            cursor: "pointer",
        }
    },
    valueContainer: (provided) => {
        return {
            ...provided,
            paddingLeft: "0",
        }
    },
    singleValue: (provided) => {
        return {
            ...provided,
            marginLeft: "0"
        }
    }

};

let DropDownIconStyle = {
    display: "inline-block",
    position: "relative",
    top: "6px",
    right: "-3px",
    fill: "rgba(128,128,128,.73)"
};

@observer
export default class NoLabelSelectComponent extends React.Component {
    constructor(props) {
        super(props);
        this.store = new ESelectStore({conf: this.props.conf, onChange: props.onChange})

        // убираем часть компонентов
        this.selectComponents = {
            //     SingleValue: this.makeSingleValueComponent(),
            DropdownIndicator: null,
            //     indicatorSeparator: null,
            LoadingIndicator: null,
            LoadingMessage: LoadingMessage,
            NoOptionsMessage: NoOptions

        }
    }

    makeSingleValueComponent() {
        // Новый компонент одиночного значения без бордеров и тд.

        return ({children, ...props}) => {
            return (
                <components.SingleValue {...props}>
                    <span style={{fontWeight: 600}}
                    >{this.store.conf.label}: </span>
                    {children}
                    <DropDownIcon {...this.props}/>
                </components.SingleValue>
            )
        };
    }

    render() {
        return (
            <AsyncPaginate
                value={this.store.getValue}
                loadOptions={this.store.loadOptions}
                defaultOptions
                debounceTimeout={this.store.debounceTimeout}
                // cacheUniq

                components={this.selectComponents}
                isClearable={false}
                isSearchable={false}

                styles={customStyles}
                isDisabled={this.store.isFetching}

                shouldLoadMore={this.store.shouldLoadMore}
                onChange={(vals, actions, e) => this.store.addValue(vals, actions, e)}
                closeMenuOnSelect={!(this.props.conf.isMulti || false)}
                placeholder={""}
                additional={{
                    page: 1,
                }}
            />

        )
    }
}