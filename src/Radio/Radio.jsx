import React from "react";
import "./Radio.scss"

class RadioList extends React.Component {
    render() {
        return (
            <div className="m-radio-list">
                {this.props.children}
            </div>
        )
    }

}


class RadioInput extends React.Component {
    render() {
        return (
            <label className="m-radio">
                <input type="radio"
                       disabled={this.props.disabled}
                       onChange={(e) => this.props.onChange()}
                       checked={this.props.checked}/>
                {this.props.label}
                <span/>
            </label>
        )

    }
}


export {RadioInput, RadioList}
