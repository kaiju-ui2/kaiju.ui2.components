import React from "react";
import "./BooleanCheckbox.scss"
import {uuid4} from "utils";

export default class BooleanCheckboxComponent extends React.Component {
    //iphone button-like component
    constructor(props) {
        super(props);
        this.id = uuid4();
        this.onChange = this.props.onChange;
        this.defaultChecked = this.props.defaultChecked === true;
    }

    render() {
        return (
            <div className={this.props.className || ""}>
                <input id={this.id} type="checkbox" disabled={this.props.disabled}
                       defaultChecked={this.defaultChecked} className="active-action ios8-switch ios8-switch-lg"
                       onChange={e => this.onChange(e.target.checked)}/>
                <label htmlFor={this.id} className="checkbox-label mr-auto">
                    {this.props.label}
                </label>

            </div>
        )
    }
}