import React from "react";
import Axios from "axios";
import {getTranslation, handleNotifyExceptions} from "utils";
import Icon from "../Icon";

export default class SearchBox extends React.Component {

    constructor(props) {
        super(props);

        this.callback = this.props.callback;
        this.timeout = props.timeout || 300;
        this.timeoutFunc = undefined;
        this.placeholder = this.props.placeholder || getTranslation("Placeholder.search")
    }

    searchCallback = (value) => {
        if (this.timeoutFunc) {
            clearTimeout(this.timeoutFunc)
        }

        this.timeoutFunc = setTimeout(() => {
            this.callback(value)
        }, this.timeout)
    };


    render() {
        return (
            <div className={`m-list-search__form-wrapper ${this.props.className}`} style={{display: "table", width: "100%"}}>
                <Icon id='search'/>
                <input className="m-list-search__form-input search-input"
                       disabled={this.props.disabled}
                       placeholder={this.placeholder} autoComplete="off"
                       style={{width: "80%"}}
                       onChange={e => this.onChange(e)}/>
            </div>
        )
    }

    onChange(event) {
        if (this.callback) {
            this.searchCallback(event.target.value)
        }
    }
}

class SearchComponent extends SearchBox {
    isFetching = false;
    inputRef;
    suggestionsRef;

    constructor(props) {
        super(props);
        this.ref = React.createRef();
        this.suggestionsComponent = (this.props.suggestionConf || {}).Component;
        this.state = {suggestions: null}
    }

    onInputChange(value) {
        if (this.callback) {
            this.searchCallback(value)
        }
        this.fetchSuggestions(value);
    }

    onSuggestionClick(suggestion) {
        this.ref.value = suggestion;
        if (this.callback) {
            this.searchCallback(suggestion)
        }
        this.cleanSuggestions();
    }

    fetchSuggestions(value) {
        let suggestionConf = this.props.suggestionConf;

        if (this.isFetching || !suggestionConf) {
            return;
        }

        let {request, key} = suggestionConf;

        if (!value) {
            this.cleanSuggestions();
            return
        }

        let {method, params = {}} = request;
        params[key || "query"] = value;
        this.isFetching = true;

        Axios.post("/public/rpc",
            {
                "method": method,
                "params": params
            }
        ).then(response => {
            this.setState({suggestions: response.data.result});
        }).catch(response => {
            handleNotifyExceptions(response)
        }).finally(() => this.isFetching = false)
    }

    cleanSuggestions() {
        this.setState({suggestions: null})
    }

    componentDidMount() {
        document.addEventListener('mouseup', (e) => this.handleClickOutside(e));
    }

    handleClickOutside(event) {
        if (!this.state.suggestions) {
            return
        }

        if (this.inputRef && this.inputRef.contains(event.target)) {
            return
        }

        if (this.suggestionsRef && !this.suggestionsRef.contains(event.target)) {
            this.cleanSuggestions()
        }
    }

    render() {
        return (
            <div
                ref={ref => this.inputRef = ref}
                className={`m-list-search__form-wrapper ${this.props.className || ""}`}
                style={{display: "table", width: "120%"}}>
                <Icon id='search' className='pr-3'/>

                <input className="m-list-search__form-input search-input"
                       ref={(ref) => this.ref = ref}
                       disabled={this.props.disabled}
                       defaultValue={this.props.defaultValue}
                       placeholder={this.placeholder} autoComplete="off"
                       style={{width: "80%"}}
                       onFocus={(e) => this.fetchSuggestions(e.target.value)}
                       onChange={e => {
                           this.onInputChange(e.target.value)
                       }}/>

                {
                    this.suggestionsComponent &&
                    <div ref={ref => this.suggestionsRef = ref}>
                        <this.suggestionsComponent
                            onClick={(text) => this.onSuggestionClick(text)}
                            suggestions={this.state.suggestions}/>
                    </div>
                }
            </div>
        )
    }

}


export {SearchComponent}