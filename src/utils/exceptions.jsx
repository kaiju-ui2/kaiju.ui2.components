import {notifyError} from "notifications";
import {getTranslation} from "./index";

let parseException = (response) => {
    if (!response && !response.data) {
        return;
    }

    let data = response.data;
    let baseMsg;

    try {
        baseMsg = data.error.data["base_exc_data"];

        if (baseMsg) {
            baseMsg=baseMsg["code"]
        } else {
            baseMsg =  data.error.data["code"]
        }

    } catch (e) {
        baseMsg = undefined;
        console.log("Не удалось распарсить exception: ", data)
    }

    return baseMsg
};

let handleNotifyExceptions = function (response) {
    let message;
    console.log("Error: ", response);

    if (!response) {
        notifyError();
        return;
    }

    message = response.message ? response.message : null;

    if (!response.data) {
        notifyError(null, message);
        return
    }

    let code = parseException(response);
    notifyError(null, code ? getTranslation(code): undefined );
};

export {handleNotifyExceptions, parseException}